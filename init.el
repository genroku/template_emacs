;;; init.el --- Configuration file for emacs.
;;; Commentary:
; init.el for linux/macosx
; last modified 2014/11/16 (since 2014/05/31)
;

;;; Code:
;======================================================================
; GENERAL SETTINGS
;======================================================================
;---<KEY BINDINGS>-----------------------------------------------------
(global-set-key [(M \?)] 'help)
(global-set-key [(C h)] 'backward-delete-char)
(global-set-key [(C c)(C r)] 'toggle-read-only)

;(C v), (C S v)はscroll-in-placeに割り当てるのでコメントアウト
;(global-set-key [(C v)] 'scroll-up)
;(global-set-key [(C S v)] 'scroll-down)
;(global-set-key [(C v)] 'forward-paragraph)
;(global-set-key [(C S v)] 'backward-paragraph)

(global-set-key [(C \')] 'dabbrev-expand)
(global-set-key [(C s)] 'isearch-forward-regexp)
(global-set-key [(C r)] 'isearch-backward-regexp)

; minibuffer でヒストリを使う
(define-key minibuffer-local-map [(C p)] 'previous-history-element)
(define-key minibuffer-local-map [(C n)] 'next-history-element)
;(define-key minibuffer-local-ns-map [(C p)] 'previous-history-element)
;(define-key minibuffer-local-ns-map [(C n)] 'next-history-element)
(define-key minibuffer-local-completion-map [(C p)] 'previous-history-element)
(define-key minibuffer-local-completion-map [(C n)] 'next-history-element)
(define-key minibuffer-local-must-match-map [(C p)] 'previous-history-element)
(define-key minibuffer-local-must-match-map [(C n)] 'next-history-element)


;---<OTHERS>-----------------------------------------------------------
(setq mouse-yank-at-point t)
(setq next-line-add-newlines nil)     ;ファイル末尾にCRを自動挿入しない
(setq kill-whole-line nil)            ;C-kで行末の改行も削除
(put 'narrow-to-region 'disabled nil) ;enable narrowing-widing
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(defvar display-time-24hr-format t)   ;default nil
(display-time)                        ;モードラインに時間を表示

(column-number-mode t)                ;モードラインにカラム番号を表示

(if                                   ;abbrev fileの読み込み
    (file-exists-p abbrev-file-name)
    (read-abbrev-file))




;======================================================================
; MODE SPECIFIC SETTINGS
;======================================================================
;---<Text Mode>--------------------------
(add-hook 'text-mode-hook
    '(lambda ()
        (auto-fill-mode nil)
        ))

;---<Emacs-lisp Mode>--------------------
(add-hook 'emacs-lisp-mode-hook
    '(lambda ()
        (setq truncate-lines t) ;画面の右端で文字を折り返さない
        (setq truncate-partial-width-windows t) ;画面の右端で文字を折り返さない(ウィンドウ分割時)
        (flycheck-mode t)
        ))

;---<Ruby Mode>--------------------------
(add-hook 'ruby-mode-hook
    '(lambda()
        (flycheck-mode t)
        ))


;---<MISCELLANEOUS>----------------------------------------------------
(show-paren-mode)                    ;括弧の対応を可視化
(line-number-mode t)                 ;モードラインへの行番号の表示
(setq-default indent-tabs-mode nil)
; TABコードはスペースで置き換える．TABコード入力にはC-q TABを使う．
(setq-default tab-width 4)




;======================================================================
; ENVIRONMENT SPECIFIC SETTINGS
;======================================================================
(if
    window-system

    (progn
      ;; default dirを~/にする.
      (setq default-directory "~/")
      ;; default dirを~/にする.Dock起動時-psnオプション付かないため必要
      (setq command-line-default-directory "~/")
      ;; スタートアップ画面を表示しないようにする
      (setq inhibit-startup-message t)
      ;; ツールバーを表示しないようにする（Official Emacs の場合は 0）
      (tool-bar-mode 0)
      ;; 行間隔を少し広げる
      (set-default 'line-spacing 4)
      ;; ウィンドウ（フレーム）のサイズ設定する
      (setq default-frame-alist '((width . 80) (height . 48)))
      ;; 背景を透過させる
      (set-frame-parameter nil 'alpha '(95 70))))

;---<set by emacs>-----------------------------------------------------
(if
    window-system

    (progn
      (custom-set-variables
       ;; custom-set-variables was added by Custom.
       ;; If you edit it by hand, you could mess it up, so be careful.
       ;; Your init file should contain only one such instance.
       ;; If there is more than one, they won't work right.
       '(ansi-color-names-vector ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
       '(column-number-mode t)
       '(custom-enabled-themes (quote (deeper-blue)))
       '(display-time-mode t)
       '(show-paren-mode t)
     
       (custom-set-faces
        ;; custom-set-faces was added by Custom.
        ;; If you edit it by hand, you could mess it up, so be careful.
        ;; Your init file should contain only one such instance.
        ;; If there is more than one, they won't work right.
       ))))

;;; init.el ends here
